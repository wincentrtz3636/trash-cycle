import { Component } from '@angular/core';
import { NavController , ModalController } from 'ionic-angular';
import { VoucherProvider } from '../../providers/voucher/voucher';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
} from '@ionic-native/google-maps';
import { ScanPage } from '../scan/scan';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  map : GoogleMap;
  point : any;
  scannedCode = null;
  constructor(public navCtrl: NavController, public modalCtrl: ModalController, private barcodeScanner: BarcodeScanner, public VoucherProvider : VoucherProvider) {
    
  }

  ionViewDidLoad(){
    this.initMap();
    this.getPoints()
  }

  getPoints(){
    this.VoucherProvider.getPoints().then(result =>{
      this.point = result;
      console.log(result);
    });
  }
  scanCode() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
    }, (err) => {
        console.log('Error: ', err);
    });
  }

  presentModalScanner() {
    const modal = this.modalCtrl.create(ScanPage);
    modal.present();
  }

  initMap(){
    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: -6.229609,
           lng: 106.827364
         },
         zoom: 18
       }
    };

    this.map = new GoogleMap('map', mapOptions);
    let marker: Marker = this.map.addMarkerSync({
      title: 'Ionic',
      icon: 'blue',
      animation: 'DROP',
      position: {
        lat: -6.229609,
        lng: 106.827364
      }
    });
    marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
      alert('clicked');
    });
  }

}
