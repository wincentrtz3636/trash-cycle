import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ToastController, ViewController} from 'ionic-angular';

/**
 * Generated class for the ScanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html',
})
export class ScanPage {
  private scanSub: any;
  constructor(public toastCtrl: ToastController, public navCtrl: NavController, public navParams: NavParams , private viewCtrl : ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScanPage');
  }

  dismissScanner() {
    let data = { 'foo': 'bar' };
    // this.hideCamera();
    this.viewCtrl.dismiss(data);
  }

}
