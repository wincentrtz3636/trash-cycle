import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { VoucherProvider } from '../../providers/voucher/voucher'


@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  datas : any;
  point : any;
  constructor(public navCtrl: NavController, public VoucherProvider : VoucherProvider) {
    
  }

  ionViewDidLoad(){
    this.getBackpacks()
    this.getPoints()
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getPoints();
      refresher.complete();

    }, 2000);
  }
  getBackpacks(){
    this.VoucherProvider.getVouchers().then(result =>{
      this.datas = result;
      console.log(result);
    });
  }

  getPoints(){
    this.VoucherProvider.getPoints().then(result =>{
      this.point = result;
      console.log(result);
    });
  }
}
