import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { VoucherProvider } from '../../providers/voucher/voucher'

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  point : any;
  constructor(public navCtrl: NavController, public VoucherProvider : VoucherProvider) {

  }

  doRefreshAbout(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getPoints();
      refresher.complete();
      
    }, 2000);
  }

  ionViewDidLoad(){
    this.getPoints()
  }

  getPoints(){
    this.VoucherProvider.getPoints().then(result =>{
      this.point = result;
      console.log(result);
    });
  }
}
