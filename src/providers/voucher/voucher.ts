import { HttpClient } from '@angular/common/http';
import { Http, Headers,RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

/*
  Generated class for the VoucherProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VoucherProvider {

  constructor(public http: HttpClient , public https : Http) {
    console.log('Hello VoucherProvider Provider');
  }

  getVouchers(){
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({
      headers: headers
    });
    return new Promise(resolve => {
      this.https.get('https://angelhack.byteforceid.com/api/vouchers', options).subscribe(data => {
        resolve(data.json());
      }, err => {
        console.log(err);
      });
    });
  }

  getPoints(){
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({
      headers: headers
    });
    return new Promise(resolve => {
      this.https.get('https://angelhack.byteforceid.com/api/points', options).subscribe(data => {
        resolve(data.json());
      }, err => {
        console.log(err);
      });
    });
  }
}
